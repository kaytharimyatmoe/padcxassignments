package com.ktmm.padcx.implict_intent_assignment

import android.app.Activity
import android.app.SearchManager
import android.content.Intent
import android.icu.util.Calendar
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.AlarmClock
import android.provider.CalendarContract
import android.provider.ContactsContract
import android.view.View
import android.widget.MediaController
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val REQUEST_CODE_CONTACT = 10
    val REQUEST_CODE_VIDEO = 11

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val beginTime = Calendar.getInstance()
        beginTime.set(2020, 1, 3, 7, 30)

        val endTime = Calendar.getInstance()
        endTime.set(2020, 1, 3, 8, 30)

        btnTimer.setOnClickListener { startTimer(getString(R.string.timer_message), 2) }
        btnCalendar.setOnClickListener {
            addEvent(
                getString(R.string.event_title),
                beginTime.timeInMillis,
                endTime.timeInMillis
            )
        }
        btnDial.setOnClickListener { dial("09790428136") }
        btnPickVideo.setOnClickListener { playVideo() }
        btnPickContact.setOnClickListener { pickContact() }
        btnWebSearch.setOnClickListener { searchWeb("Wingardium Leviosa") }
    }

    private fun startTimer(msg: String, seconds: Int) {
        val intent = Intent(AlarmClock.ACTION_SET_TIMER).apply {
            putExtra(AlarmClock.EXTRA_MESSAGE, msg)
            putExtra(AlarmClock.EXTRA_LENGTH, seconds)
            putExtra(AlarmClock.EXTRA_SKIP_UI, true)
        }
        resolveActivity(intent)
    }

    private fun addEvent(title: String, begin: Long, end: Long) {
        val intent = Intent(Intent.ACTION_INSERT).apply {
            data = CalendarContract.Events.CONTENT_URI
            putExtra(CalendarContract.Events.TITLE, title)
            putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, begin)
            putExtra(CalendarContract.EXTRA_EVENT_END_TIME, end)
        }
        resolveActivity(intent)
    }

    private fun dial(phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel:$phoneNumber")
        }
        resolveActivity(intent)
    }

    private fun playVideo() {
        val intent =
            Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        if (intent.resolveActivity(packageManager) != null)
            startActivityForResult(intent, REQUEST_CODE_VIDEO)
    }

    private fun pickContact() {
        val intent = Intent(Intent.ACTION_PICK).apply {
            type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        }
        if (intent.resolveActivity(packageManager) != null)
            startActivityForResult(intent, REQUEST_CODE_CONTACT)
    }

    private fun searchWeb(query: String) {
        val intent = Intent(Intent.ACTION_WEB_SEARCH).apply {
            putExtra(SearchManager.QUERY, query)
        }
        resolveActivity(intent)
    }

    private fun resolveActivity(intent: Intent) {
        if (intent.resolveActivity(packageManager) != null)
            startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_CONTACT -> {
                    val contactUri = data?.data
                    val projection = arrayOf(
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.NUMBER
                    )
                    val cursor = contentResolver.query(contactUri!!, projection, null, null, null)
                    if (cursor!!.moveToFirst()) {
                        val nameIndex =
                            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                        val numberIndex =
                            cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        val name = cursor.getString(nameIndex)
                        val number = cursor.getString(numberIndex)

                        tvContactInfo.visibility = View.VISIBLE
                        tvContactInfo.text = getString(R.string.contact_info, name, number)
                        videoView.visibility = View.GONE
                    }
                    cursor.close()
                }
                REQUEST_CODE_VIDEO -> {
                    val contentUri = data?.data
                    videoView.visibility = View.VISIBLE
                    tvContactInfo.visibility = View.GONE
                    val mediaController = MediaController(this)
                    mediaController.setAnchorView(videoView)
                    videoView.setMediaController(mediaController)
                    videoView.setVideoURI(contentUri)
                    videoView.requestFocus()
                    videoView.start()
                }
            }
        }
    }
}
